const estados = [
    { bandeira: "0/05/Flag_of_Brazil.svg/320px-Flag_of_Brazil.svg.png", nome: "Brasil", sigla: "BR" },
    { bandeira: "4/4c/Bandeira_do_Acre.svg/320px-Bandeira_do_Acre.svg.png", nome: "Acre", sigla: "AC" },
    { bandeira: "8/88/Bandeira_de_Alagoas.svg/320px-Bandeira_de_Alagoas.svg.png", nome: "Alagoas", sigla: "AL" },
    { bandeira: "0/0c/Bandeira_do_Amap%C3%A1.svg/320px-Bandeira_do_Amap%C3%A1.svg.png", nome: "Amapá", sigla: "AP" },
    { bandeira: "6/6b/Bandeira_do_Amazonas.svg/320px-Bandeira_do_Amazonas.svg.png", nome: "Amazonas", sigla: "AM" },
    { bandeira: "2/28/Bandeira_da_Bahia.svg/320px-Bandeira_da_Bahia.svg.png", nome: "Bahia", sigla: "BA" },
    { bandeira: "2/2e/Bandeira_do_Cear%C3%A1.svg/320px-Bandeira_do_Cear%C3%A1.svg.png", nome: "Ceará", sigla: "CE" },
    { bandeira: "3/3c/Bandeira_do_Distrito_Federal_%28Brasil%29.svg/320px-Bandeira_do_Distrito_Federal_%28Brasil%29.svg.png", nome: "Distrito Federal", sigla: "DF" },
    { bandeira: "4/43/Bandeira_do_Esp%C3%ADrito_Santo.svg/320px-Bandeira_do_Esp%C3%ADrito_Santo.svg.png", nome: "Espírito Santo", sigla: "ES" },
    { bandeira: "b/be/Flag_of_Goi%C3%A1s.svg/320px-Flag_of_Goi%C3%A1s.svg.png", nome: "Goiás", sigla: "GO" },
    { bandeira: "4/45/Bandeira_do_Maranh%C3%A3o.svg/320px-Bandeira_do_Maranh%C3%A3o.svg.png", nome: "Maranhão", sigla: "MA" },
    { bandeira: "0/0b/Bandeira_de_Mato_Grosso.svg/320px-Bandeira_de_Mato_Grosso.svg.png", nome: "Mato Grosso", sigla: "MT" },
    { bandeira: "6/64/Bandeira_de_Mato_Grosso_do_Sul.svg/320px-Bandeira_de_Mato_Grosso_do_Sul.svg.png", nome: "Mato Grosso do Sul", sigla: "MS" },
    { bandeira: "f/f4/Bandeira_de_Minas_Gerais.svg/320px-Bandeira_de_Minas_Gerais.svg.png", nome: "Minas Gerais", sigla: "MG" },
    { bandeira: "0/02/Bandeira_do_Par%C3%A1.svg/320px-Bandeira_do_Par%C3%A1.svg.png", nome: "Pará", sigla: "PA" },
    { bandeira: "b/bb/Bandeira_da_Para%C3%ADba.svg/320px-Bandeira_da_Para%C3%ADba.svg.png", nome: "Paraíba", sigla: "PB" },
    { bandeira: "9/93/Bandeira_do_Paran%C3%A1.svg/320px-Bandeira_do_Paran%C3%A1.svg.png", nome: "Paraná", sigla: "PR" },
    { bandeira: "5/59/Bandeira_de_Pernambuco.svg/320px-Bandeira_de_Pernambuco.svg.png", nome: "Pernambuco", sigla: "PE" },
    { bandeira: "3/33/Bandeira_do_Piau%C3%AD.svg/320px-Bandeira_do_Piau%C3%AD.svg.png", nome: "Piauí", sigla: "PI" },
    { bandeira: "7/73/Bandeira_do_estado_do_Rio_de_Janeiro.svg/320px-Bandeira_do_estado_do_Rio_de_Janeiro.svg.png", nome: "Rio de Janeiro", sigla: "RJ" },
    { bandeira: "3/30/Bandeira_do_Rio_Grande_do_Norte.svg/320px-Bandeira_do_Rio_Grande_do_Norte.svg.png", nome: "Rio Grande do Norte", sigla: "RN" },
    { bandeira: "6/63/Bandeira_do_Rio_Grande_do_Sul.svg/320px-Bandeira_do_Rio_Grande_do_Sul.svg.png", nome: "Rio Grande do Sul", sigla: "RS" },
    { bandeira: "f/fa/Bandeira_de_Rond%C3%B4nia.svg/320px-Bandeira_de_Rond%C3%B4nia.svg.png", nome: "Rondônia", sigla: "RO" },
    { bandeira: "9/98/Bandeira_de_Roraima.svg/320px-Bandeira_de_Roraima.svg.png", nome: "Roraima", sigla: "RR" },
    { bandeira: "1/1a/Bandeira_de_Santa_Catarina.svg/320px-Bandeira_de_Santa_Catarina.svg.png", nome: "Santa Catarina", sigla: "SC" },
    { bandeira: "2/2b/Bandeira_do_estado_de_S%C3%A3o_Paulo.svg/320px-Bandeira_do_estado_de_S%C3%A3o_Paulo.svg.png", nome: "São Paulo", sigla: "SP" },
    { bandeira: "b/be/Bandeira_de_Sergipe.svg/320px-Bandeira_de_Sergipe.svg.png", nome: "Sergipe", sigla: "SE" },
    { bandeira: "f/ff/Bandeira_do_Tocantins.svg/320px-Bandeira_do_Tocantins.svg.png", nome: "Tocantins", sigla: "TO" },
    { bandeira: "2/2f/Flag_of_the_United_Nations.svg/320px-Flag_of_the_United_Nations.svg.png", nome: "Exterior", sigla: "ZZ" }
]

function getEstadoBySigla(sigla) {
    var estado = estados.find(element => element.sigla == sigla)
    return estado.nome
}

function getBandeiraBySigla(sigla) {
    const base_url = "https://upload.wikimedia.org/wikipedia/commons/thumb/"
    var estado = estados.find(element => element.sigla == sigla)
    return base_url + estado.bandeira
}

export { getEstadoBySigla, getBandeiraBySigla }

export default estados;