import { defineComponent, h, watch, ref } from 'vue'

import { Pie } from 'vue-chartjs'
import {
	Chart as ChartJS,
	ArcElement,
	CategoryScale,
} from 'chart.js'

ChartJS.register(ArcElement, CategoryScale)

export default defineComponent({
	name: 'PieChart',
	components: {
		Pie
	},
	props: {
		data: { type: Array, default: [0, 0] },
		width: { type: Number, default: 60 }

	},

	setup(props) {
		watch(() => props.data, (newValue) => {
			chartData = { datasets: [{ data: newValue }] }
		});

		let chartData = {
			datasets: [
				{
					backgroundColor: ['red', 'blue'],
					data: props.data,
					borderWidth: 0
				}
			]
		}

		const chartOptions = {
			responsive: true,
			maintainAspectRatio: true
		}

		return () =>
			h(Pie, {
				chartData,
				chartOptions,
				width: props.width
			})
	}
})
