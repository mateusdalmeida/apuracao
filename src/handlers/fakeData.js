const axios = require('axios').default;

function randomNumber(min, max) {
    return (Math.random() * (max - min) + min) | 0;
}

function getPerc(partialValue, totalValue) {
    return ((100 * partialValue) / totalValue).toFixed(2).replaceAll(".", ",");
}


async function getApuracaoEstado(sigla) {
    await new Promise(r => setTimeout(r, 3000));

    const url = `https://resultados.tse.jus.br/oficial/ele2022/545/dados-simplificados/${sigla.toLowerCase()}/${sigla.toLowerCase()}-c0001-e000545-r.json`

    try {
        // var response = await axios.get(url);
        let p = randomNumber(10, 90)
        let votosTotais = randomNumber(0, 20000000)
        let votosA = randomNumber(0, votosTotais)
        let votosB = votosTotais - votosA

        let response = {
            data: {
                cand: [{ n: 13, vap: votosA, pvap: getPerc(votosA, votosTotais) }, { n: 22, vap: votosB, pvap: getPerc(votosB, votosTotais) }]
            }
        }

        let candidatos = []

        let data = {
            hora: response.data.ht == '' ? response.data.hg : response.data.ht,
            percentual: response.data.psi,
            votos: response.data.vv
        }

        data = {
            hora: `${randomNumber(17, 23)}:${randomNumber(10, 49)}:${randomNumber(10, 49)}`,
            percentual: `${randomNumber(10, 49)},${randomNumber(10, 49)}`,
            votos: votosTotais
        }

        response.data.cand.forEach(element => {
            let temp = {
                numero: element.n,
                votos: element.vap,
                percentual: element.pvap
            }
            if (element.n == 13) {
                temp.nome = "Lula"
                temp.cor = 'red'
                candidatos.push(temp)
            }
            if (element.n == 22) {
                temp.nome = "Bolsonaro"
                temp.cor = 'blue'
                candidatos.push(temp)
            }
        });

        data.candidatos = candidatos.sort((a, b) => b.votos - a.votos);

        // data.candidatos = candidatos.sort(() => .5 - Math.random());
        return data
    } catch (error) {
        console.log(error);
        return null;
    }

}

export { getApuracaoEstado }