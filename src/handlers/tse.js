const axios = require('axios').default;

async function getApuracaoEstado(sigla) {
    const url = `https://resultados.tse.jus.br/oficial/ele2022/545/dados-simplificados/${sigla.toLowerCase()}/${sigla.toLowerCase()}-c0001-e000545-r.json`

    try {
        var response = await axios.get(url);

        let candidatos = []

        let data = {
            hora: response.data.ht == '' ? '00:00:00' : response.data.ht,
            percentual: response.data.pst,
            votos: response.data.vv
        }

        response.data.cand.forEach(element => {
            let temp = {
                numero: element.n,
                votos: element.vap,
                percentual: element.pvap
            }
            if (element.n == 13) {
                temp.nome = "Lula"
                temp.cor = 'red'
                candidatos.push(temp)
            }
            if (element.n == 22) {
                temp.nome = "Bolsonaro"
                temp.cor = 'blue'
                candidatos.push(temp)
            }
        });

        data.candidatos = candidatos.sort((a, b) => b.votos - a.votos);
        return data
    } catch (error) {
        console.log(error);
        return null;
    }

}

export { getApuracaoEstado }